const moment = require('moment');

module.exports = humanSmallDate => {
  if (typeof humanSmallDate !== 'string') throw new Error('Not a string');
  if (!/^\d*(s|m|d|y)$/i.test(humanSmallDate)) throw new Error('Invalid small date');
  const number = Number(humanSmallDate.slice(0, -1));
  const amount = humanSmallDate[humanSmallDate.length - 1];
  return new Date(moment().add(number, amount).format());
};